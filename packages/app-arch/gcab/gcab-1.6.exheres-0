# Copyright 2017-2020 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require gnome.org [ suffix=tar.xz ] \
    meson \
    vala [ vala_dep=true with_opt=true ]

SUMMARY="GObject library and tool to create and manipulate cabinet files"
HOMEPAGE="https://wiki.gnome.org/msitools"

LICENCES="LGPL-2.1"
SLOT="0"
PLATFORMS="~amd64 ~armv8"
MYOPTIONS="
    gobject-introspection
    gtk-doc
    vapi [[ requires = [ gobject-introspection ] ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config[>=0.9.0]
        gtk-doc? ( dev-doc/gtk-doc[>=1.14] )
    build+run:
        dev-libs/glib:2[>=2.62.0]
        sys-libs/zlib
        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=0.9.4] )
"

MESON_SRC_CONFIGURE_PARAMS=(
    -Dinstalled_tests=false
    -Dnls=true
)
MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    'gobject-introspection introspection'
    'gtk-doc docs'
    vapi
)
MESON_SRC_CONFIGURE_TESTS=(
    '-Dtests=true -Dtests=false'
)

src_configure() {
    require_utf8_locale

    meson_src_configure
}

