# Copyright 2008 Saleem Abdulrasool <compnerd@compnerd.org>
# Copyright 2016-2017 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

SUMMARY="A library for making GPG easier to use"
HOMEPAGE="https://www.gnupg.org/related_software/${PN}"
DOWNLOADS="mirror://gnupg/${PN}/${PNV}.tar.bz2"

LICENCES="
    || ( GPL-2 LGPL-2.1 )
"
SLOT="0"
PLATFORMS="~amd64 ~armv7 ~armv8 ~x86"
MYOPTIONS=""

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        app-crypt/gnupg[>=2.0.4]
        dev-libs/libassuan:=[>=2.4.2]
        dev-libs/libgpg-error[>=1.36]
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/${PN}-posix-io.c-Use-off_t-instead-of-off64_t.patch
)

DEFAULT_SRC_COMPILE_PARAMS=(
    CC_FOR_BUILD=$(exhost --build)-cc
)

src_configure() {
    # additional available language bindings: JavaScript, python, python2, python3
    local languages="cl,cpp"
    econf \
        --enable-languages="${languages}" \
        --disable-gpg-test \
        --disable-gpgsm-test \
        --with-libassuan-prefix=/usr/$(exhost --target) \
        --with-libgpg-error-prefix=/usr/$(exhost --target)
}

src_test() {
    # Allow the tests to run its own instance of gpg-agent
    esandbox allow_net "unix:${WORK}/tests/gpg/S.gpg-agent*"
    esandbox allow_net --connect "unix:${WORK}/tests/gpg/S.gpg-agent*"
    esandbox allow_net "unix:${WORK}/tests/gpgsm/S.gpg-agent*"
    esandbox allow_net --connect "unix:${WORK}/tests/gpgsm/S.gpg-agent*"
    esandbox allow_net "unix:${WORK}/tests/gpg*/S.dirmngr*"
    esandbox allow_net --connect "unix:${WORK}/tests/gpg*/S.dirmngr*"

    default

    esandbox disallow_net --connect "unix:${WORK}/tests/gpg*/S.dirmngr*"
    esandbox disallow_net "unix:${WORK}/tests/gpg*/S.dirmngr*"
    esandbox disallow_net --connect "unix:${WORK}/tests/gpgsm/S.gpg-agent*"
    esandbox disallow_net "unix:${WORK}/tests/gpgsm/S.gpg-agent*"
    esandbox disallow_net --connect "unix:${WORK}/tests/gpg/S.gpg-agent*"
    esandbox disallow_net "unix:${WORK}/tests/gpg/S.gpg-agent*"
}

