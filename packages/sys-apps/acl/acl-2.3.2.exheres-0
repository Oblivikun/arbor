# Copyright 2009 Wulf C. Krueger <philantrop@exherbo.org>
# Copyright 2024 Ali Polatel <alip@chesswob.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'acl-2.2.47.ebuild' from Gentoo, which is:
#     Copyright 1999-2008 Gentoo Foundation

require flag-o-matic
require autotools [ supported_autoconf=[ 2.7 ] supported_automake=[ 1.16 ] ]

SUMMARY="Access control list utilities, libraries and headers"
HOMEPAGE="https://savannah.nongnu.org/projects/${PN}/"
DOWNLOADS="mirror://savannah/${PN}/${PNV}.tar.xz"

LICENCES="LGPL-2.1"
SLOT="0"
PLATFORMS="~amd64 ~arm ~armv7 ~armv8 ~x86"
MYOPTIONS="
    ( linguas: de es fr gl ka pl sv )
"

DEPENDENCIES="
    build:
        sys-devel/gettext[>=0.19.8]
    build+run:
        sys-apps/attr[>=2.4]
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-nls
    --enable-shared
    --disable-static
)

src_prepare() {
    expatch "${FILES}"/${PNV}-syd3.patch

    # Two tests fail with coreutils[-acl] but depending on it would create a
    # dependency cycle
    if ! has_version sys-apps/coreutils[acl] ; then
        edo sed \
            -e "/test\/cp.test/d" \
            -e "/test\/misc.test/d" \
            -i test/Makemodule.am
    fi

    # These tests assume existence of users other than paludisbuild,
    # which is not the case in user ns so tests fail with EINVAL.
    if [[ $(esandbox api) == 3 ]]; then
        edo sed \
            -e "/test\/setfacl-X.test/d" \
            -e "/test\/misc.test/d" \
            -i test/Makemodule.am
    fi

    autotoools_src_prepare
}

src_install() {
    default

    # We don't install static libraries
    edo rm "${IMAGE}"/usr/$(exhost --target)/lib/libacl.la
}

