Source/Upstream: Yes, fixed in git master

From dfc65f2d1f239256c201c68b70a5d08e6afa879f Mon Sep 17 00:00:00 2001
From: =?UTF-8?q?Robert-Andr=C3=A9=20Mauchin?= <zebob.m@gmail.com>
Date: Sun, 14 Apr 2024 11:09:20 +0200
Subject: [PATCH] a52 plugin: add support for FFMPEG 7.0
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit

channel_layout has been replace with ch_layout

avcodec_close has been deprecated in favor of avcodec_free_context

Fixes: https://github.com/alsa-project/alsa-plugins/issues/57
Closes: https://github.com/alsa-project/alsa-plugins/pull/58
Signed-off-by: Robert-André Mauchin <zebob.m@gmail.com>
Signed-off-by: Jaroslav Kysela <perex@perex.cz>
---
 a52/pcm_a52.c           | 33 ++++++++++++++++++++++++++++++++-
 rate-lav/rate_lavrate.c | 24 ++++++++++++++++++++++--
 2 files changed, 54 insertions(+), 3 deletions(-)

diff --git a/a52/pcm_a52.c b/a52/pcm_a52.c
index 43a35e7..cd56abb 100644
--- a/a52/pcm_a52.c
+++ b/a52/pcm_a52.c
@@ -82,6 +82,9 @@
 #define av_frame_free avcodec_free_frame
 #endif
 
+#define HAVE_AVCODEC_FREE_CONTEXT (LIBAVCODEC_VERSION_INT >= AV_VERSION_INT(55, 69, 100))
+#define HAVE_CH_LAYOUT (LIBAVUTIL_VERSION_INT >= AV_VERSION_INT(57, 28, 100))
+
 struct a52_ctx {
 	snd_pcm_ioplug_t io;
 	snd_pcm_t *slave;
@@ -632,7 +635,11 @@ static int a52_stop(snd_pcm_ioplug_t *io)
 static void a52_free(struct a52_ctx *rec)
 {
 	if (rec->avctx) {
-		avcodec_close(rec->avctx);
+		#if HAVE_AVCODEC_FREE_CONTEXT
+			avcodec_free_context(&rec->avctx);
+		#else
+			avcodec_close(rec->avctx);
+		#endif
 		av_free(rec->avctx);
 		rec->avctx = NULL;
 	}
@@ -671,6 +678,21 @@ static void a52_free(struct a52_ctx *rec)
 static void set_channel_layout(snd_pcm_ioplug_t *io)
 {
 	struct a52_ctx *rec = io->private_data;
+#if HAVE_CH_LAYOUT
+	switch (io->channels) {
+	case 2:
+		rec->avctx->ch_layout = (AVChannelLayout)AV_CHANNEL_LAYOUT_STEREO;
+		break;
+	case 4:
+		rec->avctx->ch_layout = (AVChannelLayout)AV_CHANNEL_LAYOUT_QUAD;
+		break;
+	case 6:
+		rec->avctx->ch_layout = (AVChannelLayout)AV_CHANNEL_LAYOUT_5POINT1;
+		break;
+	default:
+		break;
+	}
+#else
 	switch (io->channels) {
 	case 2:
 		rec->avctx->channel_layout = AV_CH_LAYOUT_STEREO;
@@ -684,6 +706,7 @@ static void set_channel_layout(snd_pcm_ioplug_t *io)
 	default:
 		break;
 	}
+#endif
 }
 #else
 #define set_channel_layout(io) /* NOP */
@@ -699,8 +722,12 @@ static int alloc_input_buffer(snd_pcm_ioplug_t *io)
 	rec->frame->nb_samples = rec->avctx->frame_size;
 #if LIBAVCODEC_VERSION_INT >= AV_VERSION_INT(58, 91, 0)
 	rec->frame->format = rec->avctx->sample_fmt;
+#if HAVE_CH_LAYOUT
+	av_channel_layout_from_mask(&rec->frame->ch_layout, rec->avctx->ch_layout.u.mask);
+#else
 	rec->frame->channels = rec->avctx->channels;
 	rec->frame->channel_layout = rec->avctx->channel_layout;
+#endif
 	if (av_frame_get_buffer(rec->frame, 0))
 		return -ENOMEM;
 #else
@@ -735,7 +762,11 @@ static int a52_prepare(snd_pcm_ioplug_t *io)
 
 	rec->avctx->bit_rate = rec->bitrate * 1000;
 	rec->avctx->sample_rate = io->rate;
+#if HAVE_CH_LAYOUT
+	rec->avctx->ch_layout.nb_channels = io->channels;
+#else
 	rec->avctx->channels = io->channels;
+#endif
 	rec->avctx->sample_fmt = rec->av_format;
 
 	set_channel_layout(io);
diff --git a/rate-lav/rate_lavrate.c b/rate-lav/rate_lavrate.c
index f78eea5..ee53e45 100644
--- a/rate-lav/rate_lavrate.c
+++ b/rate-lav/rate_lavrate.c
@@ -18,11 +18,24 @@
 #include <alsa/pcm_rate.h>
 
 #include <libswresample/swresample.h>
+#include <libavcodec/avcodec.h>
+#include <libavutil/avutil.h>
 #include <libavutil/channel_layout.h>
 #include <libavutil/opt.h>
 #include <libavutil/mathematics.h>
 #include <libavutil/samplefmt.h>
 
+/* some compatibility wrappers */
+#ifndef AV_VERSION_INT
+#define AV_VERSION_INT(a, b, c) (((a) << 16) | ((b) << 8) | (c))
+#endif
+#ifndef LIBAVUTIL_VERSION_INT
+#define LIBAVUTIL_VERSION_INT  AV_VERSION_INT(LIBAVUTIL_VERSION_MAJOR, \
+LIBAVUTIL_VERSION_MINOR, \
+LIBAVUTIL_VERSION_MICRO)
+#endif
+
+#define HAVE_CH_LAYOUT (LIBAVUTIL_VERSION_INT >= AV_VERSION_INT(57, 28, 100))
 
 static unsigned int filter_size = 16;
 
@@ -95,10 +108,17 @@ static int pcm_src_init(void *obj, snd_pcm_rate_info_t *info)
 		if (!rate->avr)
 			return -ENOMEM;
 
+#if HAVE_CH_LAYOUT
+		AVChannelLayout layout;
+		av_channel_layout_default(&layout, rate->channels);
+		av_opt_set_chlayout(rate->avr, "in_chlayout", &layout, 0);
+		av_opt_set_chlayout(rate->avr, "out_chlayout", &layout, 0);
+#else
 		av_opt_set_channel_layout(rate->avr, "in_channel_layout",
-					  av_get_default_channel_layout(rate->channels), 0);
+						av_get_default_channel_layout(rate->channels), 0);
 		av_opt_set_channel_layout(rate->avr, "out_channel_layout",
-					  av_get_default_channel_layout(rate->channels), 0);
+						av_get_default_channel_layout(rate->channels), 0);
+#endif
 		av_opt_set_int(rate->avr, "in_sample_rate", rate->in_rate, 0);
 		av_opt_set_int(rate->avr, "out_sample_rate", rate->out_rate, 0);
 		fmt = support_multi_format(rate) ? info->in.format : SND_PCM_FORMAT_S16;
