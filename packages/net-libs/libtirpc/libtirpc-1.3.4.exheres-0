# Copyright 2010-2023 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require sourceforge

SUMMARY="Transport-Independent RPC library"
DESCRIPTION="
Libtirpc is a port of Suns Transport-Independent RPC library to Linux.
It's being developed by the Bull GNU/Linux NFSv4 project.
"
HOMEPAGE+=" https://git.linux-nfs.org/?p=steved/${PN}.git"

LICENCES="BSD-3"
SLOT="0"
PLATFORMS="~amd64 ~arm ~armv7 ~armv8 ~x86"
MYOPTIONS="
    kerberos
    kerberos? ( ( providers: heimdal krb5 ) [[ number-selected = exactly-one ]] )
"

DEPENDENCIES="
    build+run:
        kerberos? (
            providers:heimdal? ( app-crypt/heimdal )
            providers:krb5? ( app-crypt/krb5 )
        )
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-ipv6
    --disable-authdes
    --disable-static
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    'kerberos gssapi'
)

src_install() {
    default

    insinto /etc
    doins doc/netconfig
}

