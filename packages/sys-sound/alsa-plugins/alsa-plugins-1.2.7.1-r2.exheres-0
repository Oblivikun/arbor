# Copyright 2008 Alexander Færøy <eroyf@exherbo.org>
# Copyright 2009 Bo Ørsted Andresen <zlin@exherbo.org>
# Copyright 2009 Ali Polatel <alip@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'alsa-plugins-1.0.19.ebuild' from Gentoo, which is:
#     Copyright 1999-2009 Gentoo Foundation

require flag-o-matic ffmpeg [ with_opt=true ]

SUMMARY="The Advanced Linux Sound Architecture (ALSA) Plugins"
HOMEPAGE="https://www.alsa-project.org"
DOWNLOADS="mirror://alsaproject/${PN#alsa-}/${PNV}.tar.bz2"

LICENCES="GPL-2 LGPL-2.1"
SLOT="0"
PLATFORMS="~amd64 ~armv7 ~armv8 ~x86"
MYOPTIONS="
    jack
    libsamplerate
    pulseaudio
    speex
"

# TODO: https://github.com/AVnu/libavtp
DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        sys-sound/alsa-lib[>=1.1.6]
        jack? ( media-sound/jack-audio-connection-kit[>=0.98] )
        libsamplerate? ( media-libs/libsamplerate )
        pulseaudio? ( media-sound/pulseaudio[>=0.9.11] )
        speex? ( media-libs/speex[>=1.2_rc] )
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/dfc65f2d1f239256c201c68b70a5d08e6afa879f.patch
)

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-arcamav
    --enable-mix
    --enable-oss
    --enable-usbstream
    --disable-aaf
    --disable-maemo-plugin
    --disable-maemo-resource-manager
    --disable-static
    --with-alsadatadir=/usr/share/alsa
    --with-alsagconfdir=/usr/share/alsa/alsa.conf.d
    --with-alsalconfdir=/etc/alsa/conf.d
    --with-plugindir=/usr/$(exhost --target)/lib/alsa-lib
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    'ffmpeg a52'
    'ffmpeg lavrate'
    'ffmpeg libav'
    jack
    'libsamplerate samplerate'
    pulseaudio
    'speex speexdsp'
)

src_prepare() {
    # NOTE(somasis): rpath is needed to fix dlopen errors on musl
    append-flags "-Wl,-rpath=/usr/$(exhost --target)/lib/alsa-lib" "-DHAVE_STDINT_H"

    default
}

src_install() {
    default

    # set the pulseaudio plugin as default output for applications using alsa
    if option pulseaudio ; then
        edo mv "${IMAGE}"/etc/alsa/conf.d/99-pulseaudio-default.conf{.example,}
    fi

    edo cd doc
    dodoc README-arcam-av README-pcm-oss upmix.txt vdownmix.txt
    option ffmpeg && dodoc a52.txt lavrate.txt
    option jack && dodoc README-jack
    option libsamplerate && dodoc samplerate.txt
    option pulseaudio && dodoc README-pulse
    option speex && dodoc speexdsp.txt speexrate.txt
}

