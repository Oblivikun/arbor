# Copyright 2020 Danilo Spinella <oss@danyspin97.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=${PN} pn=${PN}-cpp ] cmake
require flag-o-matic

export_exlib_phases pkg_setup

SUMMARY="A collection of C++ code designed to augment the C++ standard library"

LICENCES="Apache-2.0"
SLOT="0"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        dev-cpp/gtest
"

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DABSL_BUILD_TEST_HELPERS:BOOL=TRUE
    -DABSL_ENABLE_INSTALL:BOOL=ON
    -DABSL_USE_EXTERNAL_GOOGLETEST:BOOL=ON
    -DABSL_USE_SYSTEM_INCLUDES=OFF
    -DBUILD_SHARED_LIBS:BOOL=TRUE
    -DCMAKE_CXX_STANDARD=17
)

CMAKE_SRC_CONFIGURE_TESTS=(
    '-DABSL_BUILD_TESTING:BOOL=ON -DABSL_BUILD_TESTING:BOOL=OFF'
    '-DBUILD_TESTING:BOOL=ON -DBUILD_TESTING:BOOL=OFF'
)

# abseil_symbolize_test fails under syd-3 due to hardened procfs:
# https://gitlab.exherbo.org/sydbox/sydbox/-/issues/98
CMAKE_SRC_TEST_PARAMS+=(
    -E 'absl_any_invocable_test|absl_symbolize_test'
)

abseil_pkg_setup() {
    # Dynamic libraries linking to abseil need it to be compiled with -fPIC
    # https://github.com/abseil/abseil-cpp/issues/225
    append-flags -fPIC
}

