# Copyright 2009, 2010, 2011, 2012, 2013, 2021, 2023 Ali Polatel <alip@chesswob.org>
# Copyright 2015 Wouter van Kesteren <woutershep@gmail.com>
# Distributed under the terms of the GNU General Public License v2

if ever is_scm ; then
    require gitlab [ prefix="https://gitlab.exherbo.org" user="sydbox" branch="main" pn="${PN}-1" tag="v${PV}" new_download_scheme=true ]
else
    require gitlab [ prefix="https://gitlab.exherbo.org" user="sydbox" pn="${PN}" tag="v${PV}" new_download_scheme=true ]
fi

require alternatives autotools [ supported_autoconf=[ 2.7 ] supported_automake=[ 1.16 ] ]

export_exlib_phases src_test src_install pkg_preinst pkg_postinst

SUMMARY="Sydbox, the other sandbox"
DESCRIPTION="Sydbox is a ptrace based sandbox for Linux."

LICENCES="GPL-2"
MYOPTIONS="
    debug
    seccomp [[ description = [ Enable seccomp user filter support ] ]]
"

DEPENDENCIES="
    build+run:
        dev-libs/pinktrace[>=0.9.0]
        debug? ( dev-libs/libunwind )
        seccomp? ( sys-kernel/linux-headers[>=3.5] )
"

REMOTE_IDS="freecode:${PN}"

DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    debug
    seccomp
)

sydbox_src_test() {
    if ! esandbox check 2>/dev/null; then
        default
    else
        elog "Not running tests because sydbox doesn't work under sydbox"
        elog "set PALUDIS_DO_NOTHING_SANDBOXY=1 if you want to run the tests"

        elog "As of sydbox-1.0.2, tests are installed by default."
        elog "You can use the helper utility sydtest to run the tests."
    fi
}

sydbox_src_install() {
    default

    local mver=$(ever major)
    alternatives_for ${PN} ${PN} 100  \
        /usr/$(exhost --target)/bin/${PN} ${PN}-${mver}
}

sydbox_pkg_preinst() {
    # Make the update work from the binary to symlink work
    edo pushd "${ROOT}"usr/$(exhost --target)/bin
    if [[ -e sydbox && ! -L sydbox ]];then
            edo cp sydbox temp_sydbox
            edo ln -sf temp_sydbox sydbox
    fi
    edo popd
}

sydbox_pkg_postinst() {
    # Clean up again after pkg_preinst above
    if [[ -e "${ROOT}usr/$(exhost --target)/bin/temp_sydbox" ]] ; then
        edo rm "${ROOT}usr/$(exhost --target)/bin/temp_sydbox"
    fi

    alternatives_pkg_postinst
}

