# Copyright 2007 Alexander Færøy <ahf@exherbo.org>
# Copyright 2008-2011 Ingmar Vanhassel <ingmar@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require bash-completion zsh-completion elisp [ with_opt=true source_directory=contrib ]
require setup-py [ blacklist=2 import=setuptools has_bin=true multibuild=false ]
require cargo [ rust_minimum_version=1.59 with_opt=true disable_default_features=true ]

SUMMARY="A fast, lightweight, distributed source control management system"
HOMEPAGE="https://${PN}-scm.org"
DOWNLOADS="${HOMEPAGE}/release/${PNV}.tar.gz"

REMOTE_IDS="pypi:Mercurial"

UPSTREAM_RELEASE_NOTES="
    ${HOMEPAGE}/wiki/WhatsNew [[ lang = en ]]
    ${HOMEPAGE}/wiki/UpgradeNotes [[
        lang = en
        description = [ Important notes on upgrading from previous versions ]
    ]]
    ${HOMEPAGE}/wiki/HgkExtension [[
        lang = en
        description = [ Setting up Hgk ]
    ]]
"

LICENCES="|| ( GPL-3 GPL-2 )"
SLOT="0"
PLATFORMS="~amd64 ~armv8 ~x86"
MYOPTIONS="
    emacs
    rust [[ description = [ Install extensions requiring rust ] ]]
    tk [[ description = [ Install hgk, a port to mercurial of git's gitk repository viewer ] ]]
"

# The tests take >= one hour to run and, thus, are expensive.
RESTRICT="test"

DEPENDENCIES="
    build+run:
        tk? (
            dev-lang/tcl[>=8.4]
            dev-lang/tk[>=8.4]
        )
"

src_unpack() {
    default

    if option rust; then
        edo pushd "${WORK}"/rust/hg-cpython || die
        ecargo_fetch
        edo popd
    fi
}

src_prepare() {
    setup-py_src_prepare
    edo sed -i \
        -e "/rusttargetdir = /s:'release':'$(rust_target_arch_name)', &:" \
        setup.py
}

src_compile() {
    if option rust; then
        export HGWITHRUSTEXT=cpython
        edo pushd rust/hg-cpython
        cargo_src_compile
        edo popd
    fi

    setup-py_src_compile
    elisp_src_compile
}

src_test_expensive() {
    esandbox disable_net
    edo cd tests

    # At least in mercurial-1.5.4, the upstream tests break if
    # we run them in more than one job.
    # edo ${PYTHON} ./run-tests.py -j${EXJOBS:-1} --verbose
    edo ${PYTHON} ./run-tests.py -j1 --verbose --timeout=3600
    esandbox enable_net
}

src_install() {
    if option rust; then
        export HGWITHRUSTEXT=cpython
    fi

    setup-py_src_install

    option bash-completion || edo rm -r "${IMAGE}"/usr/share/bash-completion
    option zsh-completion || edo rm -r "${IMAGE}"/usr/share/zsh

    elisp_src_install

    doman doc/hg*.?
    dodoc doc/{hg.1,hgignore.5,hgrc.5,hg-ssh.8}.html

    if option tk ; then
        dobin contrib/hgk
    fi
}

